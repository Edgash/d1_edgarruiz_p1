import 'dart:ui';

import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children:[
        Row(
          children: [
            Container(
              width: 76,
              height: 50,
              child: Text(
              "Login",
              style: TextStyle(
                fontSize: 30,
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontFamily: "Arial"),),
            margin: const EdgeInsets.only(left: 20),),
            Container(
              width: 50,
              height: 40,
              decoration: BoxDecoration(
                shape:BoxShape.circle,
                color: Colors.deepPurple,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3),),
                  ]
              ),
              margin: const EdgeInsets.only(left: 135),),
            Container(
              width: 85,
              height: 100,
              decoration: BoxDecoration(
                shape:BoxShape.circle,
                color: Colors.deepPurple,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3),),
                  ],
              ),
            ),
          ],
        ),

        Row(
          children:[
        Container(
          width: 300,
          height: 200,
          decoration: BoxDecoration(
          color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3),),
              ],
          ),

             child: Container(
                child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    prefixIcon: const Icon(Icons.person,color: Colors.deepPurple,),
                    border: OutlineInputBorder(),
                    hintText: 'Username',),),
              margin: const EdgeInsets.only(left: 20.0, top: 60, right: 20),),
              /*
              child: Container(
                child: TextField(
                obscureText: true,
                decoration: InputDecoration(
                prefixIcon: const Icon(Icons.vpn_key,color: Colors.deepPurple,),
                border: OutlineInputBorder(),
                hintText: 'Password',),),
                margin: const EdgeInsets.only(left: 20.0, top: 15, right: 20),),
                */

            margin: const EdgeInsets.only(left: 55, top: 75),
            ),
          ],
        ),

          Row(
              children:[
                Container(
                  width: 150,
                  height: 50,
              decoration: BoxDecoration(
              color: Colors.deepPurple,
              borderRadius: BorderRadius.all(Radius.circular(8.0),),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3),),
                ],
              ),
            child:Text(
              "LOGIN",
              style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: "Arial",),),
            margin: const EdgeInsets.only(left: 260, top: 130),),
               /*
                onTap: (){
                  var route = MaterialPageRoute(
                    builder: (context) => listPage(),
                  );
                  Navigator.of(context).push(route);
                },
                */
              ],
        ),
    ],
      ),
    );
  }
  }